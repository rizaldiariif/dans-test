declare namespace NodeJS {
  interface ProcessEnv {
    NEXT_SECRET: string;
    GOOGLE_CLIENT_ID: string;
    GOOGLE_CLIENT_SECRET: string;
  }
}
