/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["jobs.github.com"]
  }
}

module.exports = nextConfig
