export const calculateDate = (date: Date) => {
  const date_now = new Date();

  return Math.floor(
    (date_now.getTime() - date.getTime()) / (1000 * 60 * 60 * 24)
  );
};
