import axios from "axios";
import https from "https";

export interface Job {
  id: string;
  type: string;
  url: string;
  created_at: string;
  company: string;
  company_url: string;
  location: string;
  title: string;
  description: string;
  how_to_apply: string;
  company_logo: string;
}

const client = axios.create({
  baseURL: "https://dev3.dansmultipro.co.id",
  httpsAgent: new https.Agent({ rejectUnauthorized: false }),
});

const getJobs = async (
  keyword: string | null = null,
  location: string | null = null,
  page: number = 1
): Promise<Array<Job | null>> => {
  const response = await client.get<Array<Job | null>>(
    `/api/recruitment/positions.json`,
    {
      params: {
        description: keyword,
        location,
        page,
      },
    }
  );

  return response.data;
};

const getJobDetail = async (id: string): Promise<Job> => {
  const response = await client.get<Job>(`/api/recruitment/positions/${id}`);

  return response.data;
};

export { getJobDetail, getJobs };
