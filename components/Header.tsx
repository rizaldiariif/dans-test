import { signIn, signOut, useSession } from "next-auth/react";
import Link from "next/link";

type Props = {};

const Header = (props: Props) => {
  const { status } = useSession();

  return (
    <header className="text-white bg-black">
      <div className="container flex items-center justify-between py-4 mx-auto">
        <Link href={"/"} className="text-lg font-bold">
          Dans Jobs
        </Link>
        {status === "authenticated" ? (
          <button
            onClick={() => signOut()}
            className="px-4 py-1 text-sm font-bold text-black bg-white rounded"
          >
            Sign Out
          </button>
        ) : (
          <button
            onClick={() => signIn()}
            className="px-4 py-1 text-sm font-bold text-black bg-white rounded"
          >
            Sign In
          </button>
        )}
      </div>
    </header>
  );
};

export default Header;
