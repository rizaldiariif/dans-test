import { Job } from "@/external-api/jobs-api";
import { calculateDate } from "@/helper/calculate-date";
import Link from "next/link";
import CustomLogo from "./CustomLogo";
import Briefcase from "./icons/Briefcase";
import Clock from "./icons/Clock";
import Map from "./icons/Map";

type Props = {
  job: Job;
};

const JobItem = (props: Props) => {
  const { job } = props;

  return (
    <Link href={`/${job.id}`}>
      <div className="p-4 border transition-shadow hover:shadow rounded-md">
        {/* Name */}
        <div className="flex gap-4 items-center mb-6">
          <CustomLogo company_logo={job.company_logo || "/blur.png"} />
          <div className="">
            <p className="line-clamp-1">{job.title}</p>
            <a
              className="text-blue-400 underline"
              href={job.company_url}
              target="_blank"
            >
              {job.company}
            </a>
          </div>
        </div>

        {/* Requirements */}
        <ul className="mb-6">
          <li className="flex items-center gap-2">
            <Map />
            <p>{job.location}</p>
          </li>
          <li className="flex items-center gap-2">
            <Briefcase />
            <p>{job.type}</p>
          </li>
        </ul>

        <div className="flex items-center gap-2 text-slate-500">
          <Clock />
          <p className="text-sm">
            Posted {calculateDate(new Date(job.created_at))} days ago
          </p>
        </div>
      </div>
    </Link>
  );
};

export default JobItem;
