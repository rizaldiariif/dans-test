import { getJobs } from "@/external-api/jobs-api";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import JobItem from "./JobItem";
import Clock from "./icons/Clock";

type Props = {};

const JobList = (props: Props) => {
  const [jobs, setJobs] = useState<any[]>([]);
  const router = useRouter();
  const [has_more, setHasMore] = useState(true);

  const [page, setPage] = useState(1);

  const fetchData = async () => {
    if (router.isReady) {
      try {
        const keyword = router.query.keyword as string;
        const location = router.query.location as string;
        const full_time = (router.query.full_time as string) || "true";
        let fetched_jobs = await getJobs(keyword, location, page);
        if (fetched_jobs.includes(null)) {
          setHasMore(false);
        }
        fetched_jobs = fetched_jobs.filter((job: any) => job !== null);
        if (full_time === "true") {
          fetched_jobs = fetched_jobs.filter(
            (job: any) => job.type === "Full Time"
          );
        }
        const temp_jobs = Array.from(jobs);
        temp_jobs.push(...fetched_jobs);
        setJobs(temp_jobs);
      } catch (error) {
        console.error(error);
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, [page, router.isReady]);

  return (
    <div className="container mx-auto my-8">
      <InfiniteScroll
        dataLength={jobs.length}
        hasMore={has_more}
        next={() => {
          setPage(page + 1);
          fetchData();
        }}
        loader={
          <div className="flex justify-center items-center text-center p-10">
            <Clock />
          </div>
        }
        endMessage={
          <div className="flex justify-center items-center text-center p-10">
            <p>No More Jobs to Load.</p>
          </div>
        }
      >
        <div className="grid lg:grid-cols-2 gap-4">
          {jobs.map((job) => (
            <JobItem key={job.id} job={job} />
          ))}
        </div>
      </InfiniteScroll>
    </div>
  );
};

export default JobList;
