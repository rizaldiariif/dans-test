type Props = {};

const Footer = (props: Props) => {
  return (
    <footer className="text-white bg-black">
      <div className="container flex items-center justify-center py-4 mx-auto">
        <p className="text-xs font-semibold">
          © 2023 Company, Inc. All rights reserved
        </p>
      </div>
    </footer>
  );
};

export default Footer;
