import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import BasicInput from "./BasicInput";

type Props = {};

const SearchToolBar = (props: Props) => {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = handleSubmit(async (data) => {
    const { keyword, location, full_time } = data;
    const param_object: any = {};

    if (keyword !== null && keyword !== "" && keyword !== "null") {
      param_object.keyword = keyword;
    }

    if (location !== null && location !== "" && location !== "null") {
      param_object.location = location;
    }

    if (full_time !== null && full_time !== "" && full_time !== "null") {
      param_object.full_time = full_time;
    }

    param_object.page = 1;

    window.location.replace(`/?${new URLSearchParams(param_object)}`);
  });

  return (
    <div className="container mx-auto my-8">
      <h2 className="text-3xl font-semibold mb-4">
        Look for your job together with Us!
      </h2>
      <form onSubmit={onSubmit}>
        <div className="flex gap-6 flex-col lg:flex-row">
          <BasicInput
            error={errors.keyword}
            field_name="keyword"
            register={register}
            placeholder="Your Job Keyword Here..."
            validation={{
              required: false,
            }}
            default_value={router.query.keyword}
          />
          <BasicInput
            error={errors.location}
            field_name="location"
            register={register}
            placeholder="Your Job Location Preference Here..."
            container_additional_class="flex-grow"
            validation={{
              required: false,
            }}
            default_value={router.query.location}
          />
          <div className="flex gap-2 items-center">
            <input
              type="checkbox"
              {...register("full_time", {
                value: router.query.full_time === "true",
              })}
            />
            <label htmlFor="full_time">Full Time Only</label>
          </div>
          <input
            type="submit"
            value="Cari Pekerjaan"
            className="w-full px-20 py-2 text-sm font-semibold text-white uppercase bg-black border rounded-md cursor-pointer lg:w-auto"
          />
        </div>
      </form>
    </div>
  );
};

export default SearchToolBar;
