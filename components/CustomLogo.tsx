import Image from "next/image";
import { useState } from "react";

type Props = {
  company_logo: string;
};

const CustomLogo = (props: Props) => {
  const { company_logo } = props;
  const [image_url, setImageURL] = useState(company_logo);

  return (
    <div className="w-12 h-12 relative">
      <Image
        src={image_url}
        placeholder={"blur"}
        blurDataURL="/blur.png"
        fill={true}
        alt="Company Logo"
        onError={() => {
          setImageURL("/blur.png");
        }}
      />
    </div>
  );
};

export default CustomLogo;
