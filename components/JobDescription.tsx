type Props = {
  job_description: string;
};

const JobDescription = (props: Props) => {
  const { job_description } = props;

  return (
    <div className="my-8">
      <h2 className="text-2xl font-bold mb-8">Job Description</h2>
      <div
        className="description-container mb-10"
        dangerouslySetInnerHTML={{ __html: job_description }}
      />
    </div>
  );
};

export default JobDescription;
