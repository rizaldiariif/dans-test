import BasicInput, { InputItemOption } from "./BasicInput";

export type InputItemProps = {
  field_name: string;
  label: string;
  placeholder?: string;
  type?: string;
  default_value?: string;
  validation?: Object;
  option?: boolean;
  options?: InputItemOption[];
  wysiwyig?: boolean;
  setValue?: any;
  textarea?: any;
};

type Props = {
  onSubmit: any;
  input_fields: InputItemProps[];
  register: any;
  errors: any;
};

const BasicForm = (props: Props) => {
  const { onSubmit, input_fields, register, errors } = props;

  return (
    <form onSubmit={onSubmit}>
      {input_fields.map((input) => (
        <BasicInput
          field_name={input.field_name}
          label={input.label}
          type={input.type}
          register={register}
          error={errors[input.field_name]}
          default_value={input.default_value}
          option={input.option}
          options={input.options}
          setValue={input.setValue}
          validation={input.validation}
          textarea={input.textarea}
          placeholder={input.placeholder}
          key={input.field_name}
        />
      ))}
      <input
        type="submit"
        value="Simpan"
        className="w-full px-20 py-2 text-sm font-semibold text-white uppercase bg-black border rounded-md cursor-pointer border-jankos-pink-300 lg:w-auto"
      />
    </form>
  );
};

export default BasicForm;
