import { useState } from "react";

export type InputItemOption = {
  value: string | number;
  label: string;
};

type Props = {
  label?: string | null;
  field_name: string;
  type?: string;
  validation?: Object;
  placeholder?: string;
  register: any;
  setValue?: any;
  error: any;
  default_value?: any;
  option?: boolean;
  options?: InputItemOption[];
  textarea?: boolean;
  disabled?: boolean;
  additional_class?: string;
  container_additional_class?: string;
  label_additional_class?: string;
  pattern?: string | null;
  title?: string | null;
};

const BasicInput = ({
  label = null,
  field_name,
  type = "text",
  placeholder,
  register,
  setValue,
  error,
  default_value = null,
  validation = {
    required: {
      value: true,
      message: `${label || field_name} harus diisi`,
    },
  },
  option = false,
  options = [],
  textarea = false,
  disabled = false,
  additional_class = "",
  container_additional_class = "",
  label_additional_class = "",
  pattern = null,
  title = null,
}: Props) => {
  const [show_password, setShowPassword] = useState<boolean>(false);

  return (
    <div className={`relative flex flex-col ${container_additional_class}`}>
      {/* {label && (
        <label
          htmlFor={field_name}
          className={`text-sm ${label_additional_class}`}
        >
          {label}
        </label>
      )} */}
      {!option && !textarea && (
        <>
          <input
            type={show_password ? "text" : type}
            placeholder={placeholder || label}
            className={`rounded-lg border border-slate-300 bg-white text-black py-2 px-6 disabled:bg-slate-100 ${additional_class}`}
            disabled={disabled}
            pattern={pattern}
            title={title}
            {...register(field_name, {
              value: default_value,
              ...validation,
            })}
          />
          {type === "password" && (
            <span
              className="absolute right-2.5 top-2.5 cursor-pointer"
              onClick={setShowPassword.bind(this, !show_password)}
            >
              {!show_password ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"
                  />
                </svg>
              )}
            </span>
          )}
        </>
      )}
      {textarea && (
        <textarea
          rows={3}
          placeholder={placeholder || label}
          className="px-6 py-2 border rounded-lg border-slate-300 bg-white text-black placeholder:pt-1 placeholder:text-xs"
          {...register(field_name, {
            value: default_value,
            ...validation,
          })}
        ></textarea>
      )}
      {option && (
        <select
          className="block px-6 py-2 border rounded-lg border-slate-300 bg-white text-black"
          {...register(field_name, {
            value: default_value,
            ...validation,
          })}
        >
          <option value="">{placeholder || `Pilih ${label}`}</option>
          {options.map((op) => (
            <option key={op.value} value={op.value}>
              {op.label}
            </option>
          ))}
        </select>
      )}
      {error && <p className="mt-1 text-xs text-red-500">{error.message}</p>}
    </div>
  );
};

export default BasicInput;
