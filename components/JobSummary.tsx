import { Job } from "@/external-api/jobs-api";
import { calculateDate } from "@/helper/calculate-date";
import CustomLogo from "./CustomLogo";
import Building from "./icons/Building";
import Clock from "./icons/Clock";
import Map from "./icons/Map";

type Props = {
  job: Job;
};

const JobSummary = (props: Props) => {
  const { job } = props;

  return (
    <div className="flex gap-4 flex-col lg:flex-row my-8">
      <CustomLogo company_logo={job.company_logo || "/blur.png"} />
      <div className="flex-grow">
        <h1 className="text-xl mb-1">{job.title}</h1>
        <a
          className="text-blue-500 underline mb-6 block"
          href={job.company_url}
        >
          {job.company}
        </a>
        <ul className="flex flex-col gap-2">
          <li className="flex flex-row items-center gap-2">
            <Building />
            {job.type}
          </li>
          <li className="flex flex-row items-center gap-2">
            <Map />
            {job.location}
          </li>
        </ul>

        <div className="flex flex-row items-center gap-2 text-slate-500 my-6">
          <Clock />
          <p className="text-sm">
            Posted {calculateDate(new Date(job.created_at))} days ago
          </p>
        </div>

        <div
          className="apply-container mb-10 overflow-hidden w-80 h-20"
          dangerouslySetInnerHTML={{ __html: job.how_to_apply }}
        />
      </div>
    </div>
  );
};

export default JobSummary;
