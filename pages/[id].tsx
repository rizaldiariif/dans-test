import Footer from "@/components/Footer";
import Header from "@/components/Header";
import JobDescription from "@/components/JobDescription";
import JobSummary from "@/components/JobSummary";
import { Job, getJobDetail } from "@/external-api/jobs-api";
import { GetServerSideProps } from "next";
import { useSession } from "next-auth/react";
import Head from "next/head";
import { useRouter } from "next/router";

type Props = {
  job: Job;
};

const JobDetail = (props: Props) => {
  const { job } = props;
  const { status } = useSession();
  const router = useRouter();

  if (!router.isReady || status === "loading") {
    return null;
  }

  if (status !== "authenticated") {
    router.replace("/api/auth/signin");
  }

  return (
    <>
      <Head>
        <title>
          {job.title} | {job.company} | Dans Jobs
        </title>
      </Head>
      <Header />
      <div className="container mx-auto">
        <JobSummary job={job} />
        <hr className="my-6 block" />
        <JobDescription job_description={job.description} />
      </div>
      <Footer />
    </>
  );
};

export default JobDetail;

export const getServerSideProps: GetServerSideProps<{
  job: Job;
}> = async (context) => {
  const id = context.params?.id;

  if (typeof id !== "string") {
    throw new Error("ID is not valid");
  }

  const job = await getJobDetail(id);
  return { props: { job } };
};
