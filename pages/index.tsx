import Footer from "@/components/Footer";
import Header from "@/components/Header";
import JobList from "@/components/JobList";
import SearchToolBar from "@/components/SearchToolBar";
import { useSession } from "next-auth/react";
import Head from "next/head";
import { useRouter } from "next/router";

type Props = {};

const HomePage = (props: Props) => {
  const router = useRouter();
  const { status } = useSession();

  if (!router.isReady || status === "loading") {
    return null;
  }

  if (status !== "authenticated") {
    router.replace("/api/auth/signin");
  }

  return (
    <>
      <Head>
        <title>Dans Jobs</title>
      </Head>
      <Header />
      <SearchToolBar />
      <JobList />
      <Footer />
    </>
  );
};

export default HomePage;
