import "@/styles/globals.css";
import { SessionProvider } from "next-auth/react";
import type { AppProps } from "next/app";
import { Roboto_Mono, Rubik } from "next/font/google";

const rubik = Rubik({
  subsets: ["latin"],
  display: "swap",
  variable: "--font-Rubik",
  weight: ["300", "400", "500", "600", "700", "800", "900"],
});

const roboto_mono = Roboto_Mono({
  subsets: ["latin"],
  display: "swap",
  variable: "--font-Roboto-Mono",
  weight: ["100", "200", "300", "400", "500", "600", "700"],
});

export default function App({ Component, pageProps }: AppProps) {
  return (
    <SessionProvider session={pageProps.session}>
      <main
        className={`${rubik.variable} ${roboto_mono.variable} font-sans min-h-screen bg-white text-black`}
      >
        <Component {...pageProps} />
      </main>
    </SessionProvider>
  );
}
